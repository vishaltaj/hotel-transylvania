const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');

require('./helpers/arrayHelper');

const app = express();
const port = process.env.PORT || 5000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/hotels', (req, res) => {
  let rawData = fs.readFileSync('./db.json');  
  let response = JSON.parse(rawData);
  let hotels = {}

  response = response.groupBy('groupKey');

  Object.keys(response).map(function(hotelGroup){
    hotels[hotelGroup] = {}
    response[hotelGroup].map(function(hotel){
      let hasBreakfast = ["Breakfast", "All Inclusive"].includes(hotel.boardCodeDescription) ? 'with breakfast' : 'with no breakfast';
      let key = (hotel.bedTypeLabel || []).concat(hasBreakfast).join('|');
      (hotels[hotelGroup][key]  = hotels[hotelGroup][key] || []).push(hotel);
    })
  })

  res.send(hotels);
});
app.post('/api/book', (req, res) => {
  let response = {}
  let totalGuest = req.body.adults + req.body.children; // total valid guests
  let rooms = {};

  // guest allotments
  let adultsArray = Array.from({length: req.body.adults}, () => "adult");
  let childrenArray = Array.from({length: req.body.children}, () => "child");
  let infantsArray = Array.from({length: req.body.infants}, () => "infant");

  // get total no of rooms needed
  let roomsCountByKids = [req.body.children / 3, req.body.infants / 3];
  let totalRooms = Math.ceil(Math.max(...[Math.ceil(req.body.adults / 3), Math.max(...roomsCountByKids)]));

  // allotment algorithm 
  if ( totalGuest <= 7 && req.body.adults >= totalRooms ) {
    if (totalRooms == 1) rooms['r1'] = adultsArray.concat(childrenArray, infantsArray);
    else {
      for(i=1; i <= totalRooms; i++) {
        rooms['r'+i] = []
      }
      while ( adultsArray.length > 0 || childrenArray.length > 0 || infantsArray.length > 0 ) {
        i = 1
        while (totalRooms >= i) {
          if(adultsArray.length > 0) rooms['r'+i].push(adultsArray.pop());
          j = 1;
          while ( totalRooms >= j ) {
            if(childrenArray.length > 0) rooms['r'+j].push(childrenArray.pop());
            j++;
          }
          j = 1;
          while ( totalRooms >= j ) {
            if ( infantsArray.length > 0 ) rooms['r'+j].push(infantsArray.pop());
            j++;
          }
          i++;
        }
      }
    }
    if (Object.keys(rooms).length > 0) {
      response = { rooms: rooms };
    }
    else {
      res.status(422);
      response = {error: 'Invalid input'}
    }
  }
  else {
    res.status(422)
    response = {error: 'Invalid Booking'}
  }
  res.send(response);
});

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));
  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));
