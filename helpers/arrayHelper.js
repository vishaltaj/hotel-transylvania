Array.prototype.groupBy = function(prop) {
    return this.reduce(function(groups, item) {
      (groups[item[prop]] = groups[item[prop]] || []).push(item);
      return groups;
    }, {})
};