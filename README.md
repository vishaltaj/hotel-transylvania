## Hotel Transalvania

A simple application built using Express and React. which helps allot rooms based on no of guest per booking.

[Click here to view demo](https://hotel-booking-react.herokuapp.com)

### Setup

please install `nodemon` if it is not installed

    $ npm i nodemon -g
	
then run `yarn` to install dependency packages in for node application.

    $ yarn
    $ cd client && npm install


go to root directory of app run. 

    $ yarn dev
    
to start in development mode

For accessing Node App

[http://localhost:5000](http://localhost:5000/api/hotels)

For accessing React App

[http://localhost:3000](http://localhost:3000)