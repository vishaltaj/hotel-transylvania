import React, { Component } from 'react';

import { Book, HotelList, Header, ThankYou } from './components/index';
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends Component {
  render() {
    return(<Router>
      <div>
        <Header />
        <Route exact path="/" component={HotelList} />
        <Route path="/book" component={Book} />
        <Route path="/thank_you" component={ThankYou} />
      </div>
    </Router>);
  }
}

export default App;
