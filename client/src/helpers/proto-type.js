Boolean.prototype.humanize = function() { return this === true ? 'Yes' : 'No' }


Array.prototype.groupCount = function() {
    return this.reduce((acum,cur) => Object.assign(acum,{[cur]: (acum[cur] | 0)+1}), {})
};