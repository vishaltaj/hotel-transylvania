
import React, { Component } from 'react';

import { Link } from "react-router-dom";
import '../../css/Header.scss';

class Header extends Component {
    render() {
        return(
            <header>
                <nav className="navbar navbar-light bg-light">
                    <Link to={{ pathname: '/'}} className="navbar-brand mb-0 h1">Hotel Transylvania</Link>
                </nav>
            </header>);
    }
}

export default Header;

