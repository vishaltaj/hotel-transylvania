import React, { Component } from 'react';
import { 
    Button, 
    FormControl, 
    FormGroup,
    ControlLabel,
    InputGroup,
    Alert } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default  class Book extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            hotel_id: this.props.location.state.hotel_id, 
            name: '',
            adultCount: 0,
            infantCount: 0,
            childrenCount: 0,
            roomCount: 0,
            message: '',
            messageColor: '',
            alertIsHidden: true,
            rooms: {}
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name] : target.value, alertIsHidden: true })
    }

    increment = (ref) => this.setState({ [ref] : this.state[ref] + 1, alertIsHidden: true});

    decrement = (ref) => {
        if (this.state[ref] === 0) return false;
        this.setState({ [ref] : this.state[ref] - 1, alertIsHidden: true});
    }

    handleSubmit = async e => {
        e.preventDefault();
        const response = await fetch('/api/book', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ name: this.state.name,
            adults: this.state.adultCount,
            children: this.state.childrenCount,
            hotel_id: this.state.hotel_id,
            infants: this.state.infantCount
         })   
        });

        const body = await response.json();
        if (response.status !== 200) {
            this.setState({ message: body.error, messageColor: 'danger', alertIsHidden: false })
        }
        else {
            this.state.rooms = body.rooms
            this.setState({ message: JSON.stringify(body.rooms), messageColor: 'success', alertIsHidden: false })
        }
    };

    render() {

        if(Object.keys(this.state.rooms).length > 0) {
            return <Redirect to={{pathname: 'thank_you', state: {rooms: this.state.rooms}}}/>
        }

        return(
        <div className="container">
            <div className="row">
                <legend className="col-xs-12">Booking Details</legend>
                <div className="col-xs-12">
                    <Alert bsStyle={this.state.messageColor} className={this.state.alertIsHidden ? 'hidden' : 'show'}> 
                        { this.state.message }
                    </Alert>
                </div>
                <form onSubmit={this.handleSubmit} className="col-xs-12">
                    <FormGroup
                    controlId="formBasicText"
                    className="col-xs-12"
                    >
                        <ControlLabel>Name</ControlLabel>
                        <FormControl
                            type="text"
                            value={this.state.name}
                            name="name"
                            placeholder="Enter text"
                            onChange={ this.handleChange }
                        />
                    </FormGroup>
                    <FormGroup className="col-xs-12 col-md-2">
                        <ControlLabel>No Of Adults</ControlLabel>
                        <InputGroup>
                            <InputGroup.Addon onClick={() => this.decrement('adultCount')}>-</InputGroup.Addon>
                            <FormControl type="text" value={this.state.adultCount} name="adults" />
                            <InputGroup.Addon onClick={() => this.increment('adultCount')}>+</InputGroup.Addon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup className="col-xs-12 col-md-2">
                        <ControlLabel>No Of Children</ControlLabel>
                        <InputGroup>
                            <InputGroup.Addon onClick={() => this.decrement('childrenCount')}>-</InputGroup.Addon>
                            <FormControl type="text" value={this.state.childrenCount} name="children" />
                            <InputGroup.Addon onClick={() => this.increment('childrenCount')}>+</InputGroup.Addon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup className="col-xs-12 col-md-2">
                        <ControlLabel>No Of Infants</ControlLabel>
                        <InputGroup>
                            <InputGroup.Addon onClick={() => this.decrement('infantCount')}>-</InputGroup.Addon>
                            <FormControl type="text" value={this.state.infantCount} name="infants" />
                            <InputGroup.Addon onClick={() => this.increment('infantCount')}>+</InputGroup.Addon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup className="col-xs-12">
                        <Button type="submit" class="btn btn-default">Submit</Button>
                    </FormGroup>
                </form>
            </div>
        </div>);
    }
}



