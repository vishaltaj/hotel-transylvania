import Book from './Book';
import HotelList from './HotelList';
import ThankYou from './ThankYou';
import Header from './shared/Header';

export  { Book, HotelList, Header, ThankYou };

