import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Link } from "react-router-dom";

import '../helpers/proto-type';

export default class Hotel extends Component {

    constructor(props) {
        super(props);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {modalShow: false};
    }


    handleClose() {
        this.setState({ modalShow: false });
    }

    handleShow() {
        this.setState({ modalShow: true });
    }

    render() {
        return (
            <div className="row hotel-group">
                <div className="col-md-6"><b>{ this.props.name }</b></div>
                <div className="col-md-4"><strong>{ this.props.price } MYR</strong></div>
                <div className="col-md-2">
                    <Button bsStyle="default" bsSize="small" onClick={this.handleShow}>
                        View More
                    </Button>
                </div>
                
                <Modal show={this.state.modalShow} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="hotel">
                        <div className="row">
                            <div className="col-xs-12">
                                {this.props.description }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-md-3">
                                <legend>Breakfast</legend> {this.props.hasBreakfast.humanize() }
                            </div>
                            <div className="col-xs-12 col-md-3">
                                <legend>Total Price</legend> { this.props.price }
                            </div>
                            <div className="col-xs-12 col-md-3">
                                <legend>Promo Price</legend> { this.props.promoprice }
                            </div>
                            <div className="col-xs-12 col-md-3">
                                <legend>Refundable</legend> { this.props.isNonRefundable.humanize() }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-md-3">
                                <legend>Can Hold Booking</legend> { this.props.canHoldBooking.humanize() }
                            </div>
                            <div className="col-xs-12 col-md-3">
                                <legend>Booking hold date</legend> { this.props.cancelFreeBeforeDate }
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Link to={{ pathname: '/book', state: {hotel_id: this.props.id} }} params={{ hotel_id: this.props.id }} className="btn btn-default btn-sm">Book</Link>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}