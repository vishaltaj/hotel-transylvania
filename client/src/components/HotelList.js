import React, { Component } from 'react';
import Hotel from './Hotel'

import '../css/HotelList.scss';
// import { __await } from 'tslib';


export default class HotelList extends Component {

  constructor(props) {
    super(props);

    this.state = { hotelByTypes: {} };
  }

  componentWillMount() {
    this.getHotels()
      .then(res => this.setState({ hotelByTypes: res }))
      .catch(err => console.log(err));
  }

  getHotels = async () => {
    const response = await fetch('/api/hotels');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  }

  render() {
    let { hotelByTypes } = this.state;

    return (
      <div className="App">
        <div className="container">
          { Object.keys(hotelByTypes).map(function(hotelType) {
              return (<div className="row">
                <div className="col-md-12">
                  <h4>{ hotelType }</h4>
                </div>
                <div className="col-md-12 hotel-list">
                  { Object.keys(hotelByTypes[hotelType]).map(function(hotelBySubType) {
                     return (
                       <div className="row">
                         <h5 className="sub-heading"><i className="glyphicon glyphicon-tags"></i> { hotelBySubType.split('|').join(' ') }</h5>
                         <div className="col-xs-12">
                            { hotelByTypes[hotelType][hotelBySubType].map(function(hotel) {
                              return <Hotel key={hotel.id} {... hotel} />;
                            }) }
                          </div>
                       </div>
                    );
                  }) }
                </div>
              </div>)
          }) 
          }
        </div>
      </div>
    );
  }
}
