import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../css/ThankYou.scss';
import '../helpers/proto-type';

export default class ThankYou extends Component {
    constructor(props) {
        super(props);
        this.state = {rooms: this.props.location.state.rooms, roomCount: []}
    }

    render() {
        let { rooms, roomCount } = this.state;
        return(
            <div className="container">
                <div className="row">
                    <div className="col-xs-12">
                        <h3>Thank You!</h3>
                        <p>For using our service. below you can see the alloted rooms</p>
                    </div>
                    <div id="rooms" className="col-xs-12">
                        { Object.keys(rooms).map(function(room, index){
                            roomCount = rooms[room].groupCount()
                            return(
                                <div className="room" key={index}>
                                    <h5><strong>Room{ index+1 }</strong></h5>
                                    <ul>
                                        <li><img src="/images/adult.svg" alt="adult"/><span>X {roomCount['adult']}</span></li>
                                        <li><img src="/images/child.svg" alt="child"/><span>X {roomCount['child']}</span></li>
                                        <li><img src="/images/infant.svg" alt="infant"/><span>X {roomCount['infant']}</span></li>
                                    </ul>
                                </div>
                                )
                            }) 
                        }
                    </div>
                    <div className="col-xs-12">
                        <ul id="note">
                            <li><img src="/images/adult.svg" alt="adult"/> Adult</li>
                            <li><img src="/images/child.svg" alt="child"/> Child</li>
                            <li><img src="/images/infant.svg" alt="infant"/> Infant</li>
                        </ul>
                    </div>
                    <div className="col-xs-12" style={{'margin-top': '2pc'}}>
                        <Link to="/" className="btn btn-default">Back to listing</Link>
                    </div>
                </div>
            </div>);
    }
}

